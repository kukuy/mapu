package org.kukuy.mapu.services

import org.kukuy.mapu.domain.*

class CityService {

    private val cities: Map<String, String> = mapOf(
        BA_NAME to BA_API_ID,
        LH_NAME to LH_API_ID,
        BRL_NAME to BRL_API_ID)

    fun getCityID(baName: String): String? {
        return cities[baName]
    }
}