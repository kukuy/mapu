package org.kukuy.mapu


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import org.kukuy.mapu.domain.API_KEY
import org.kukuy.mapu.domain.CITY_API_ID
import org.kukuy.mapu.domain.CityWeather
import org.kukuy.mapu.services.NetworkService

class MainActivity : AppCompatActivity() {

    private var city: CityWeather = CityWeather.emptyCityWeather()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val cityID = intent.getStringExtra(CITY_API_ID)
        doApiWeatherRequest(cityID)
    }

    private fun doApiWeatherRequest(cityId:String?) {
        if(NetworkService.isOnline(this)){
            getRequest("https://api.openweathermap.org/data/2.5/weather?id=${cityId}&units=metric&appid=${API_KEY}")
        }else {
            notifyOffLine()
        }
    }

    private fun notifyOffLine() {
        Toast.makeText(this,
            "Estás desconectado paisano",
            Toast.LENGTH_LONG).show()
    }

    private fun init() {
        tvGrades?.text = city.main.temp.toString().plus("°")
        tvCountry?.text = city.name
        tvWeather?.text = city.weather[0].main
    }

    fun getRequest(url:String){
        val queue = Volley.newRequestQueue(this)

        val request = StringRequest(Request.Method.GET, url, Response.Listener<String> {
                response -> handleResponse(response)
        }, Response.ErrorListener{
                error ->  handleRequestError(error)
        })

        queue.add(request)
    }

    private fun handleResponse(response:String) {
        try {
            Log.d("getRequestSuccess", response)
            val gson = Gson()
            city = gson.fromJson(response, CityWeather::class.java)
            init()
        } catch (e: Exception) {
            Log.d("getRequestError", response)
        }
    }

    private fun handleRequestError(error: VolleyError?) {
        Log.d("getRequestError", error.toString())
    }
}
