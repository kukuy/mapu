package org.kukuy.mapu.domain

class Main(temp:Float) {
    var temp: Float = 0f

    init {
        this.temp = temp
    }

    companion object{
        fun emptyMain(): Main{
            return Main(0f)
        }
    }
}
