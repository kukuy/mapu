package org.kukuy.mapu.domain

import java.io.Serializable

class CityWeather(name:String, weather:List<Weather>, main:Main){
    var name: String = ""
    var weather: List<Weather> = emptyList()
    var main: Main = Main.emptyMain()

    init {
        this.name = name
        this.weather = weather
        this.main = main
    }

    companion object{
        fun emptyCityWeather(): CityWeather{
            return CityWeather("", emptyList(), Main.emptyMain())
        }
    }
}