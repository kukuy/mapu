package org.kukuy.mapu

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import org.kukuy.mapu.domain.CITY_API_ID
import org.kukuy.mapu.services.CityService
import android.os.StrictMode
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Build


class CiudadesActivity : AppCompatActivity() {

    private val cityService: CityService = CityService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ciudades)

        init()

    }

    private fun init() {
        val btnBA = findViewById<Button>(R.id.btnBA)
        val btnLH = findViewById<Button>(R.id.btnLH)
        val btnBRL = findViewById<Button>(R.id.btnBRL)

        btnBA.setOnClickListener(goMainActivity())
        btnLH.setOnClickListener(goMainActivity())
        btnBRL.setOnClickListener(goMainActivity())
    }

    private fun goMainActivity(): (View) -> Unit {
        return { it ->
            val cityName = (it as Button).text.toString()
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(CITY_API_ID, cityService.getCityID(cityName))
            startActivity(intent)
        }
    }
}
